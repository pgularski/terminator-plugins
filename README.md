Terminator plugins
==================

A set of terminator plugins - both written by me and ones found on the Internet.

Get Terminator (cross-platform GPL terminal emulator) [here](http://software.jessies.org/terminator/)

Place all plugins you want in ~/.config/terminator/plugins/

- - -

Plugins: 
--------
* **Show titlebar** - context menu with "Show titlebar" option
* **Search plugin** - Search google for the selected text in a terminal 
